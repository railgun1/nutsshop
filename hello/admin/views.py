import os

from flask import Blueprint
from flask import __version__
from flask import render_template

from hello.extensions import db
from hello.initializers import redis


admin = Blueprint("admin", __name__, template_folder="templates")


@admin.get("/admin")
def home():
    return render_template(
        "admin/pages/home.html",
        flask_ver=__version__,
        python_ver=os.environ["PYTHON_VERSION"],
        flask_env=os.environ["FLASK_ENV"],
    )
