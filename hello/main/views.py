import os

from flask import Blueprint
from flask import __version__
from flask import render_template

from hello.extensions import db
from hello.initializers import redis
from .models import Panda, Vodka

main = Blueprint("main", __name__, template_folder="templates")


@main.get("/")
def home():
    return render_template(
        "main/pages/index.html",
        flask_ver=__version__,
        python_ver=os.environ["PYTHON_VERSION"],
        flask_env=os.environ["FLASK_ENV"],
    )


@main.get("/lte")
def lte():
    return render_template(
        "main/pages/../admin/templates/admin/lte2.html",
        flask_ver=__version__,
        python_ver=os.environ["PYTHON_VERSION"],
        flask_env=os.environ["FLASK_ENV"],
    )


@main.get("/about")
def about():
    return render_template(
        "main/pages/about.html",
        flask_ver=__version__,
        python_ver=os.environ["PYTHON_VERSION"],
        flask_env=os.environ["FLASK_ENV"],
    )


@main.get("/payment")
def payment():
    return render_template(
        "main/pages/payment.html",
        flask_ver=__version__,
        python_ver=os.environ["PYTHON_VERSION"],
        flask_env=os.environ["FLASK_ENV"],
    )


@main.get("/customers")
def customers():
    return render_template(
        "main/pages/customers.html",
        flask_ver=__version__,
        python_ver=os.environ["PYTHON_VERSION"],
        flask_env=os.environ["FLASK_ENV"],
    )


@main.get("/news")
def news():
    return render_template(
        "main/pages/news.html",
        flask_ver=__version__,
        python_ver=os.environ["PYTHON_VERSION"],
        flask_env=os.environ["FLASK_ENV"],
    )


@main.get("/up")
def up():
    db.create_all()
    redis.ping()
    db.engine.execute("SELECT 1")

    # guest = Panda(email='guest', address='guest@example.com')
    # db.session.add(guest)
    # db.session.commit()

    panda = Panda.query.first()
    return "" + panda.email


# @page.route("/login", methods=["POST", "GET"])
# def login():
#     if 'userLogged' in session:
#         return redirect(url_for('profile', username=session['userLogged']))
#     elif request.method == "POST" and request.form['username'] == "selfedu" and request.form["psw"] == "123":
#         session["userLogged"] = request.form["username"]
#         return redirect(url_for("profile", username=session["userLogged"]))
#
#     return render_template("login.html", title="Авторизация")

# @page.get("/profile/<username>")
# def profile(username):
#     if 'userLogged' not in session or session["userLogged"] != username:
#         abort(401)
#
#     return f"Профиль пользователя {username}"


# from flask import Blueprint, render_template, abort, session, flash, redirect, url_for
#
# @store_blueprint.route('/product/<int:id>', methods=['GET', 'POST'])
# def product(id=0):
#     # AddCart is a form from WTF forms. It has a prefix because there
#     # is more than one form on the page.
#     cart = AddCart(prefix="cart")
#
#     # This is the product being viewed on the page.
#     product = Product.query.get(id)
#
#
#     if cart.validate_on_submit():
#         # Checks to see if the user has already started a cart.
#         if 'cart' in session:
#             # If the product is not in the cart, then add it.
#             if not any(product.name in d for d in session['cart']):
#                 session['cart'].append({product.name: cart.quantity.data})
#
#             # If the product is already in the cart, update the quantity
#             elif any(product.name in d for d in session['cart']):
#                 for d in session['cart']:
#                     d.update((k, cart.quantity.data) for k, v in d.items() if k == product.name)
#
#         else:
#             # In this block, the user has not started a cart, so we start it for them and add the product.
#             session['cart'] = [{product.name: cart.quantity.data}]
#
#
#         return redirect(url_for('store.index'))